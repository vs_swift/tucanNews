 //
//  WebViewController.swift
//  tucanNews
//
//  Created by Private on 1/22/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class WebViewController: UIViewController {
    
    @IBOutlet weak var webview: UIWebView!
    
    var url:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webview.loadRequest(URLRequest(url: URL(string: url!)!))
    }
}
