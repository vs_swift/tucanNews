 //
//  Article.swift
//  tucanNews
//
//  Created by Private on 1/21/18.
//  Copyright © 2018 Private. All rights reserved.
//

import UIKit

class Article: NSObject {
    
    var headline: String?
    var desc: String?
    var author: String?
    var url:String?
    var imageUrl: String?
 }
